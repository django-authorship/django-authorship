# syntax=docker/dockerfile:1
#
# Build:
# docker build --build-arg uid=${UID} --build-arg gid=${GROUPS} --tag="${PWD##*/}_django-authorship" .
#
# Run tests:
# docker run --user ${UID}:${GROUPS} --rm --tty --interactive --volume="${PWD}":/opt/project "${PWD##*/}_django-authorship"


FROM ubuntu:20.04 as base


LABEL maintainer="devops@mattaustin.com.au"


# Install required os packages
ENV DEBIAN_FRONTEND=noninteractive
RUN apt-get update --quiet --yes && \
    apt-get install --quiet --yes build-essential ca-certificates software-properties-common

RUN add-apt-repository --yes --update ppa:deadsnakes/ppa && \
    apt-get install --quiet --yes python3.6 python3.6-dev python3.6-venv python3.7 python3.7-dev python3.7-venv python3.8 python3.8-dev python3.8-venv python3.9 python3.9-dev python3.9-venv python3.10 python3.10-dev python3.10-venv


# Set up python venv
RUN /usr/bin/python3.6 -m venv /opt/venv3.6
RUN /opt/venv3.6/bin/python -m pip install --force --upgrade "pip>=21.1"
RUN /opt/venv3.6/bin/python -m pip install --force --upgrade setuptools


FROM base

# Install required python packages
RUN /opt/venv3.6/bin/python -m pip install build ipython "jedi<0.18" tox
ADD ./pyproject.toml /opt/project/
ADD ./README.rst /opt/project/
ADD ./setup.cfg /opt/project/
ADD ./src/authorship/__init__.py /opt/project/src/authorship/__init__.py
RUN /opt/venv3.6/bin/python -m pip install --cache-dir=/opt/cache --editable /opt/project "django~=2.2"
VOLUME /opt/cache


# Add code
ADD ./src /opt/project/src
ADD ./tests /opt/project/tests


# Configure environment
ENV HOME=/tmp
ENV LANG=C.UTF-8
ENV LC_ALL=C.UTF-8
ENV PYTHONPATH=/opt/project/src
ENV VIRTUAL_ENV=/opt/venv3.6


# Create user
ARG uid=1000
ARG gid=1000
RUN groupadd group --gid=$gid
RUN useradd user --uid=$uid --gid=$gid


# Defaults
WORKDIR /opt/project
USER $uid:$gid
ENTRYPOINT ["/opt/venv3.6/bin/tox"]
CMD [""]
