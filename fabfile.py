# -*- coding: utf-8 -*-
import os

import invoke


try:
    from fabric import api  # noqa: F401  # Test for fabric v1.x
except ImportError:
    # No fabric v1, so use primary module name
    from fabric import task
else:
    # Fabric v1 exists, so attempt to use fallback module name
    from fabric2 import task

try:
    from typing import Optional
except ImportError:
    pass


gid = max(os.getgid(), 1000)

local_pwd = os.path.realpath(os.path.dirname(os.path.realpath(__file__)))

project_name = os.path.split(local_pwd)[-1]

uid = max(os.getuid(), 1000)


@task  # type: ignore[misc]
def build(context, options=""):
    # type: (invoke.context.Context, Optional[str]) -> None
    with context.cd("."):
        invoke.run(
            "docker-compose build "
            "--build-arg uid={uid} --build-arg gid={gid} "
            "{options} django-authorship".format(
                uid=uid, gid=gid, options=options
            )
        )
        invoke.run(
            "docker-compose run --rm --user=0:0 --no-deps "
            "--volume={project_name}_ipython:/tmp/.ipython --entrypoint=chown "
            "django-authorship {uid}:{gid} /tmp/.ipython".format(
                uid=uid, gid=gid, project_name=project_name
            )
        )
        invoke.run(
            "docker-compose run --rm --user=0:0 --no-deps "
            "--volume={project_name}_tox:/opt/project/.tox --entrypoint=chown "
            "django-authorship {uid}:{gid} /opt/project/.tox".format(
                uid=uid, gid=gid, project_name=project_name
            )
        )


@task  # type: ignore[misc]
def bash(context, service="django-authorship", command=""):
    # type: (invoke.context.Context, Optional[str], Optional[str]) -> None
    run(context, service=service, command=command, options="--entrypoint=bash")


@task  # type: ignore[misc]
def django(context, command, options=""):
    # type: (invoke.context.Context, str, Optional[str]) -> None
    options = (
        "{options} "
        '--entrypoint="/opt/venv3.6/bin/python -m django" '
        '-e DJANGO_SETTINGS_MODULE="tests.app.settings" '
        "--volume={project_name}_ipython:/tmp/.ipython ".format(
            options=options, project_name=project_name
        )
    )
    run(context, service="django-authorship", command=command, options=options)


@task  # type: ignore[misc]
def docs(context):
    # type: (invoke.context.Context) -> None
    tox(context, envlist="docs")


@task  # type: ignore[misc]
def pip(context, command):
    # type: (invoke.context.Context, str) -> None
    python(context, command="-m pip {command}".format(command=command))


@task  # type: ignore[misc]
def python(context, command):
    # type: (invoke.context.Context, str) -> None
    run(
        context,
        service="django-authorship",
        command=command,
        options="--no-deps --entrypoint=/opt/venv3.6/bin/python",
    )


@task  # type: ignore[misc]
def run(context, service, command="", options=""):
    # type: (invoke.context.Context, str, Optional[str], Optional[str]) -> None
    with context.cd("."):
        invoke.run(
            "docker-compose run --rm --user={uid}:{gid} "
            "{options} {service} {command}".format(
                uid=uid,
                gid=gid,
                options=options,
                service=service,
                command=command,
            ),
            pty=True,
        )


@task  # type: ignore[misc]
def test(context):
    # type: (invoke.context.Context) -> None
    tox(context, envlist="python3.6-django2.2,coverage")


@task  # type: ignore[misc]
def tox(context, envlist=None):
    # type: (invoke.context.Context, Optional[str]) -> None
    command = (
        "-e {envlist}".format(envlist=envlist) if envlist else "--parallel"
    )
    run(
        context,
        service="django-authorship",
        command=command,
        options=(
            "--entrypoint=/opt/venv3.6/bin/tox "
            "--volume={project_name}_tox:/opt/project/.tox ".format(
                project_name=project_name
            )
        ),
    )


@task  # type: ignore[misc]
def wheel(context):
    # type: (invoke.context.Context) -> None
    with context.cd("."):
        invoke.run("rm -rf ./build")
    run(
        context,
        service="django-authorship",
        command="--wheel",
        options=(
            '--no-deps --entrypoint="/opt/venv3.6/bin/python -m build" '
            '-e PYTHONDONTWRITEBYTECODE="" '
        ),
    )
