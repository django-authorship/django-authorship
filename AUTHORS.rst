.. _credits:

=======
Credits
=======

* Elena Williams <git@elena.net.au>
* Josh Crompton <josh.crompton@gmail.com>
* Kye Russell <me@kye.id.au>
* Mark Lockett <mark.lockett@thecut.net.au>
* Matt Austin <devops@mattaustin.com.au>
