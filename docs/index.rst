=================
django-authorship
=================

.. toctree::
   :maxdepth: 2
   :hidden:

   installation
   usage
   authors
   history.rst
   reference/authorship.rst


Development environment
=======================
Docker, docker-compose, and fabric are used to manage the development
environment. See the `README.rst` file for instructions on setting this up.


==================
Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
