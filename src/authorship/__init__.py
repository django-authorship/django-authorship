__author__ = "Matt Austin <devops@mattaustin.com.au>"

__copyright__ = "Copyright 2017-2021"

__license__ = "Apache 2.0"

__title__ = "django-authorship"

__url__ = "https://gitlab.com/django-authorship/django-authorship/"

__version__ = "2.0.0"


default_app_config = "authorship.apps.AppConfig"
