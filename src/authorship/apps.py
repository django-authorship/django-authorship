from django import apps


class AppConfig(apps.AppConfig):

    name = "authorship"

    label = "authorship"
