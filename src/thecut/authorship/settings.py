from authorship.settings import AUTH_USER_MODEL, WEBSITE_USER


__all__ = ["AUTH_USER_MODEL", "WEBSITE_USER"]
