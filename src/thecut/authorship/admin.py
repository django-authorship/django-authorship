from authorship.admin import AuthorshipInlineMixin, AuthorshipMixin


__all__ = ["AuthorshipInlineMixin", "AuthorshipMixin"]
