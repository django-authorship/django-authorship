from authorship.views import AuthorshipMixin, AuthorshipViewMixin


__all__ = ["AuthorshipMixin", "AuthorshipViewMixin"]
