from authorship.utils import get_website_user


__all__ = ["get_website_user"]
