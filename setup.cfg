[codespell]
skip = docs/_build
summary = True

[coverage:html]
show_contexts = True

[coverage:paths]
source =
    src/
    .tox/python*/lib/python*/site-packages/

[coverage:report]
fail_under = 60

[coverage:run]
branch = True
omit =
    */migrations/*
parallel = True
source = authorship,thecut.authorship

[flake8]
max-complexity = 10

[isort]
profile = black
known_first_party = authorship,thecut.authorship
known_third_party = thecut
line_length = 79
lines_after_imports = 2

[metadata]
author = Matt Austin
author_email = devops@mattaustin.com.au
classifiers =
    Development Status :: 5 - Production/Stable
    Environment :: Web Environment
    Framework :: Django
    Framework :: Django :: 2.2
    Framework :: Django :: 3.1
    Framework :: Django :: 3.2
    Intended Audience :: Developers
    License :: OSI Approved :: Apache Software License
    Operating System :: OS Independent
    Programming Language :: Python
    Programming Language :: Python :: 3
    Programming Language :: Python :: 3.6
    Programming Language :: Python :: 3.7
    Programming Language :: Python :: 3.8
    Programming Language :: Python :: 3.9
    Programming Language :: Python :: 3.10
    Topic :: Internet :: WWW/HTTP :: Dynamic Content
    Topic :: Software Development :: Libraries :: Python Modules
    Topic :: Software Development :: Libraries :: Application Frameworks
description = A set of Django mixins to easily record authorship information for your models.
keywords =
    django
    authorship
    created
    updated
    model
    mixin
license = Apache 2.0
license_files =
    LICENSE
long_description = file: README.rst
name = django-authorship
project_urls =
    Documentation = https://django-authorship.readthedocs.io/en/latest/
    Changelog = https://django-authorship.readthedocs.io/en/latest/history.html
    Issues = https://gitlab.com/django-authorship/django-authorship/issues
    Source = https://gitlab.com/django-authorship/django-authorship
url = https://gitlab.com/django-authorship/django-authorship
version = attr: authorship.__version__

[mypy]
check_untyped_defs = True
disallow_incomplete_defs = True
disallow_untyped_calls = True
disallow_untyped_decorators = True
disallow_untyped_defs = True
mypy_path = src
namespace_packages = True
plugins =
    mypy_django_plugin.main
show_error_codes = True
warn_redundant_casts = True
warn_unused_ignores = True

[mypy-fabric.*]
ignore_missing_imports = True

[mypy-fabric2.*]
ignore_missing_imports = True

[mypy-factory.*]
ignore_missing_imports = True

[mypy-invoke.*]
ignore_missing_imports = True

[mypy-rest_framework.*]
ignore_missing_imports = True

[mypy-zoneinfo.*]
ignore_missing_imports = True

[mypy.plugins.django-stubs]
django_settings_module = "tests.app.settings"

[options]
include_package_data = True
install_requires =
    django>=2.2,!=3.0.*
package_dir=
    = src
packages = find_namespace:
python_requires = >=3.6

[options.extras_require]
docs =
    djangorestframework>=3,<4
    factory-boy~=3.2
    sphinx~=4.2
    sphinx-rtd-theme~=1.0
tests =
    coverage~=6.0
    factory-boy~=3.2
    freezegun~=1.1
    unittest-xml-reporting~=3.0
types =
    django-stubs~=1.7
    mypy==0.910

[options.package_data]
authorship = py.typed
thecut.authorship = py.typed

[options.packages.find]
where = src
