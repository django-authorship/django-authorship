=================
django-authorship
=================

.. image:: https://gitlab.com/django-authorship/django-authorship/badges/main/pipeline.svg
   :target: https://gitlab.com/django-authorship/django-authorship/commits/main

.. image:: https://gitlab.com/django-authorship/django-authorship/badges/main/coverage.svg
   :target: https://django-authorship.gitlab.io/django-authorship/coverage/

.. image:: https://readthedocs.org/projects/django-authorship/badge/?version=latest
    :target: http://django-authorship.readthedocs.io/en/latest/?badge=latest
    :alt: Documentation Status

A set of Django mixins to easily record authorship information for your models.

Features
--------

* Base model allows easy recording of authorship information.
* Integration with Django's class-based views and forms.
* Integration with Django's admin.


Documentation
-------------

The full documentation is at https://django-authorship.readthedocs.io/


Quickstart
----------

Install ``django-authorship`` using the installation instructions found in the project documentation.

Build a model based on ``authorship.models.Authorship`` to record authorship information on it::

    from authorship.models import Authorship

    class MyModel(Authorship):

        pass

This adds ``created_by``, ``created_at``, ``updated_by``, and ``updated_at`` to your model.

Pass a user into calls to ``.save()`` to record which user changed the object::

    example = MyModel()
    example.save(user=request.user)

If you need to update model data and there's no direct link to a website user, generate and use a site-wide 'generic' user.::

    from authorship.models import get_website_user

    example = MyModel()
    example.save(user=get_website_user())

If you wish to automatically record authorship information for changes made in the Django admin, use ``authorship.admin.AuthorshipMixin``.::

    from authorship.admin import AuthorshipMixin
    from django.contrib import admin

    from .models import MyModel

    @admin.register(MyModel)
    class MyModelAdmin(AuthorshipMixin, admin.ModelAdmin):

        pass

If you wish to integrate with ``django.forms.ModelForm``, use ``authorship.forms.AuthorshipMixin`` and ``authorship.views.AuthorshipMixin``.

In your ``forms.py``::

    from authorship.forms import AuthorshipMixin
    from django import forms

    from .models import MyModel

    class MyModelForm(forms.ModelForm):

        class Meta(object):
            model = MyModel

In your ``views.py``::

    from authorship.views import AuthorshipMixin
    from django.views.generic import CreateView

    from .forms import MyModelForm
    from .models import MyModel

    class MyModelCreateView(AuthorshipMixin, CreateView):

        form_class = MyModelForm

``MyModelCreateView`` will now automatically pass ``request.user`` through to ``MyModelForm``, which will pass it through to the model's `save()` method.


Credits
-------

See ``AUTHORS.rst``.
