from thecut.authorship import factories

from . import models


class AuthorshipModelFactory(factories.AuthorshipFactory):
    class Meta:
        model = models.AuthorshipModel
