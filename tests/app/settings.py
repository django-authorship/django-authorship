import warnings


DEBUG = True

DATABASES = {"default": {"ENGINE": "django.db.backends.sqlite3"}}

DEFAULT_AUTO_FIELD = "django.db.models.BigAutoField"

INSTALLED_APPS = [
    "django.contrib.auth",
    "django.contrib.contenttypes",
    "thecut.authorship",
    "tests.app",
]

SECRET_KEY = "!"  # nosec: B105

TEST_OUTPUT_DIR = "xunit"

TEST_RUNNER = "xmlrunner.extra.djangotestrunner.XMLTestRunner"

USE_TZ = True


warnings.filterwarnings(
    "error",
    r"DateTimeField .* received a naive datetime",
    RuntimeWarning,
    r"django\.db\.models\.fields",
)
