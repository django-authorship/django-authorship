from django import apps


class AppConfig(apps.AppConfig):

    name = "tests.app"

    label = "test_app"
