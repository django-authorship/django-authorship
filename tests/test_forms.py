from unittest import mock

from django import forms
from django.test import TestCase

from tests.app.models import AuthorshipModel
from thecut.authorship.factories import UserFactory
from thecut.authorship.forms import AuthorshipMixin


class AuthorshipModelForm(AuthorshipMixin, forms.ModelForm):
    class Meta:
        model = AuthorshipModel
        fields: list = []


class TestAuthorshipMixin(TestCase):
    def test_requires_an_extra_argument_on_creating_an_instance(self) -> None:
        """Ensure that
        :py:class:`thecut.authorship.forms.AuthorshipMixin`-based forms cannot
        be instantiated without passing in a user."""

        with self.assertRaises(TypeError):
            AuthorshipModelForm()  # type: ignore[call-arg]

    def test_sets_user_attribute(self) -> None:
        """Ensure that
        :py:class:`thecut.authorship.forms.AuthorshipMixin`-based forms
        properly set :py:attr:`thecut.authorship.forms.AuthorshipMixin.user`
        when one is passed on instantiation."""
        dummy_user = mock.Mock()

        form = AuthorshipModelForm(user=dummy_user)

        self.assertEqual(dummy_user, form.user)


class TestAuthorshipMixinSave(TestCase):
    @mock.patch("django.forms.ModelForm.save")
    def test_calls_super_class_save_method(self, save: mock.Mock) -> None:

        """Ensure that
        :py:meth:`thecut.authorship.forms.AuthorshipMixin.save` calls the
        superclass's save method.."""
        form = AuthorshipModelForm(user=UserFactory())
        form.instance = mock.Mock()

        form.save()

        save.assert_called_once_with()

    @mock.patch("django.forms.ModelForm.save")
    def test_sets_updated_by_to_given_user(self, *args: mock.Mock) -> None:
        """Ensure that
        :py:class:`thecut.authorship.forms.AuthorshipMixin`-based forms
        appropriately set
        :py:attr:`thecut.authorship.models.AuthorshipMixin.updated_by` when
        a user is provided."""
        user = mock.Mock()
        form = AuthorshipModelForm(user=user)
        form.instance = mock.Mock()
        form.cleaned_data = {}

        form.save()

        self.assertEqual(user, form.instance.updated_by)

    def test_sets_created_by_if_instance_is_not_saved(self) -> None:
        """Ensure that
        :py:class:`thecut.authorship.forms.AuthorshipMixin`-based forms
        appropriately set
        :py:attr:`thecut.authorship.models.AuthorshipMixin.created_by` when
        a user is provided and the target object has not been saved before."""

        user = UserFactory.build()
        form = AuthorshipModelForm(user=user)
        form.cleaned_data = {}

        form.save(commit=False)

        self.assertEqual(user, form.instance.created_by)

    @mock.patch("django.forms.ModelForm.save")
    def test_does_not_set_created_by_if_instance_is_saved(
        self, *args: mock.Mock
    ) -> None:
        """Ensure that
        :py:class:`thecut.authorship.forms.AuthorshipMixin`-based forms do
        not set
        :py:attr:`thecut.authorship.models.AuthorshipMixin.created_by` if the
        target object has already been saved."""

        class DummySavedModel:
            def __init__(self) -> None:
                self.created_at = "arbitrary-value"
                self.created_by = "arbitrary-value"

        user = mock.Mock()
        form = AuthorshipModelForm(user=user)
        form.instance = DummySavedModel()
        form.cleaned_data = {}

        form.save()

        self.assertNotEqual(user, form.instance.created_by)
