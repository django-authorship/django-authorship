from django.test import TestCase

from thecut.authorship import utils


class TestGetWebsiteUser(TestCase):
    """Tests for get_website_user()."""

    def test_get_website_user_returns_user(self) -> None:
        """Test if something is returned."""
        user = utils.get_website_user()
        self.assertTrue(user)

    def test_get_website_user_returns_same_user(self) -> None:
        """Test if the same user is returned over multiple calls."""
        user = utils.get_website_user()
        self.assertEqual(utils.get_website_user(), user)
